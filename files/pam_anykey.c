#include <security/pam_modules.h>
#include <security/pam_ext.h>
#include <stdlib.h>

PAM_EXTERN int pam_sm_authenticate(pam_handle_t *pamh, int flags, int argc, const char **argv) {
    int retval;
    char *keyword = NULL;

    retval = pam_prompt(pamh, PAM_PROMPT_ECHO_ON, &keyword, "Press ENTER to continue:");
    free(keyword);
    return PAM_SUCCESS;
}

PAM_EXTERN int pam_sm_setcred(pam_handle_t *pamh, int flags, int argc, const char **argv) {
    return PAM_SUCCESS;
}

#ifdef PAM_STATIC
struct pam_module _pam_prompt_key_modstruct = {
    "pam_prompt_key",   // Module name
    pam_sm_authenticate,  // Function to prompt for any key press
    pam_sm_setcred,
    NULL,
    NULL,
    NULL,
    NULL,
};
#endif