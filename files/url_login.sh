#!/bin/bash
set -e
if [ -z "$PAM_SERVICE" ] || [ "$PAM_SERVICE" != "sshd" ]
then
   echo "not sshd"
   exit 1
fi


for i in {1..3}
do
   if [ -f /var/local/done ]
   then
      rm -f /var/local/done
      exit 0
   fi
   sleep 1
done
echo "Authentication failed, try again."
rm -f /var/local/done
exit 1
